package co.jeeon.cordovaplugin.printimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.pdf.PdfDocument;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentInfo;
import android.print.pdf.PrintedPdfDocument;

import org.apache.cordova.CallbackContext;

import java.io.FileOutputStream;
import java.io.IOException;

public class PrintImageAdapter extends PrintDocumentAdapter {
    public enum Positioning { FIT, FILL, ABSOLUTE }

    private Context context;
    private CallbackContext command;
    private PrintedPdfDocument pdfDocument;
    private Positioning positioning;
    private Integer x;
    private Integer y;
    private Integer width;
    private Integer height;
    private String filename;
    private Bitmap bitmap;

    public PrintImageAdapter(Context context, CallbackContext command, Bitmap bitmap,
                             String filename, Positioning positioning, Integer x, Integer y,
                             Integer width, Integer height) {
        this.context = context;
        this.command = command;
        this.bitmap = bitmap;
        this.filename = filename;
        this.positioning = positioning;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public void onFinish() {
        bitmap.recycle();
        bitmap = null;
        command.success("");
        super.onFinish();
    }

    @Override
    public void onLayout(PrintAttributes oldAttributes, PrintAttributes newAttributes,
                         CancellationSignal cancellationSignal, LayoutResultCallback callback,
                         Bundle extras) {
        pdfDocument = new PrintedPdfDocument(context, newAttributes);

        if (cancellationSignal.isCanceled()) {
            callback.onLayoutCancelled();
            return;
        }

        PrintDocumentInfo info = new PrintDocumentInfo.Builder(filename)
                .setContentType(PrintDocumentInfo.CONTENT_TYPE_DOCUMENT)
                .setPageCount(1)
                .build();

        boolean changed = oldAttributes.getMediaSize() == null ||
                !oldAttributes.getMediaSize().equals(newAttributes.getMediaSize());
        callback.onLayoutFinished(info, changed);
    }

    @Override
    public void onWrite(PageRange[] pages, ParcelFileDescriptor destination,
                        CancellationSignal cancellationSignal, WriteResultCallback callback) {
        PdfDocument.Page page = pdfDocument.startPage(0);

        if (cancellationSignal.isCanceled()) {
            callback.onWriteCancelled();
            pdfDocument.close();
            pdfDocument = null;
            return;
        }

        drawPage(page);
        pdfDocument.finishPage(page);

        try {
            pdfDocument.writeTo(new FileOutputStream(destination.getFileDescriptor()));
        } catch (IOException e) {
            callback.onWriteFailed(e.toString());
            return;
        } finally {
            pdfDocument.close();
            pdfDocument = null;
        }

        callback.onWriteFinished(new PageRange[] { PageRange.ALL_PAGES });
    }

    private void drawPage(PdfDocument.Page page) {
        Canvas canvas = page.getCanvas();
        switch (positioning) {
            case FIT: resize(canvas, true); break;
            case FILL: resize(canvas, false); break;
            case ABSOLUTE: put(canvas); break;
        }
    }

    private void resize(Canvas canvas, boolean fit) {
        int bw = bitmap.getWidth();
        int bh = bitmap.getHeight();

        int cw = canvas.getWidth();
        int ch = canvas.getHeight();

        int w;
        int h;

        if (fit ? bh > bw : bh < bw) {
            float r = (float) ch / bh;
            w = (int) (r * bw);
            h = ch;
        } else {
            float r = (float) cw / bw;
            w = cw;
            h = (int) (r * bh);
        }

        canvas.drawBitmap(bitmap, new Rect(0, 0, bw, bh),
                new Rect((cw - w) >> 1, (ch - h) >> 1, (cw + w) >> 1, (ch + h) >> 1), new Paint());
    }

    private void put(Canvas canvas) {
        if (x == null) x = 0;
        if (y == null) y = 0;

        if (width == null) width = bitmap.getWidth();
        if (height == null) height = bitmap.getHeight();

        canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()),
                new Rect(x, y, x + width, y + height), new Paint());
    }
}
