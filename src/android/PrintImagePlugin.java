package co.jeeon.cordovaplugin.printimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.print.PrintManager;
import android.util.Base64;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

public class PrintImagePlugin extends CordovaPlugin {
    @Override
    public boolean execute(String action, JSONArray args, CallbackContext command)
            throws JSONException {
        if ("print".equals(action)) {
            String imageData = args.getString(0);
            byte[] imageBytes = null;
            try {
                imageBytes = Base64.decode(imageData, Base64.DEFAULT);
            } catch (Exception e) {
                command.error("INVALID_IMAGE");
                return true;
            }
            Bitmap bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            if (bitmap == null) {
                command.error("INVALID_IMAGE");
                return true;
            }
            String filename = args.getString(1);
            PrintImageAdapter.Positioning positioning =
                    PrintImageAdapter.Positioning.valueOf(args.getString(2));
            
            if (positioning == PrintImageAdapter.Positioning.ABSOLUTE) {
                Integer x = null, y = null, width = null, height = null;
                
                try { x = args.getInt(3); } catch (Exception e) {}
                try { y = args.getInt(4); } catch (Exception e) {}
                try { width = args.getInt(5); } catch (Exception e) {}
                try { height = args.getInt(6); } catch (Exception e) {}

                print(cordova.getActivity(), command, bitmap, filename,
                        positioning, x, y, width, height);
            } else {
                print(cordova.getActivity(), command, bitmap, filename,
                        positioning, null, null, null, null);
            }
            return true;
        }
        return false;
    }

    private void print(final Context context, final CallbackContext command, final Bitmap bitmap,
                       final String filename, final PrintImageAdapter.Positioning positioning,
                       final Integer x, final Integer y,
                       final Integer width, final Integer height) {
        cordova.getThreadPool().execute(new Runnable() {
            @Override
            public void run() {
                PrintManager printManager =
                        (PrintManager) context.getSystemService(Context.PRINT_SERVICE);
                PrintImageAdapter adapter = new PrintImageAdapter(context, command, bitmap,
                        filename, positioning, x, y, width, height);
                printManager.print(filename, adapter, null);
            }
        });
    }
}
