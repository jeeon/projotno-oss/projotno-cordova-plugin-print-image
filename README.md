# projotno-cordova-plugin-print-image
Cordova plugin for printing image

## Installation
```sh
cordova plugin add projotno-cordova-plugin-print-image
```

## Supported Platforms
- Android 4.4 KitKat (API Level 19)

## Usage
```js
window.plugins.printImage(
    'Image data encoded as base-64 string',
    'Filename.pdf',
    {
        positioning: 'FIT'
    },
    function () {
        // Handle print success
    },
    function (errorCode) {
        // Handle print error
    }
);
```

## Documentation
The plugin only has the following function:

```js
window.plugins.printImage(imageData, filename, options, successCallback, errorCallback);
```

Parameters:
1. `imageData`: Image data encoded as base-64 string.
2. `filename`: Default filename to be used when saving the print output as PDF.
3. `options`: Options object with structure ```{ positioning: 'FIT', 'FILL', or 'ABSOLUTE', x: integer, y: integer, width: integer, height: integer }```.
    * `'FIT'` fits the image in the printable area by resizing the image accordingly.
    * `'FILL'` fills the whole printable area by resizing the image even if some parts of the image fall outside of this area.
    * `'ABSOLUTE'` puts the image at `(x, y)` coordinate by resizing it to `(width, height)` size. The top-left corner of the page is `(0, 0)`.
    * `x`, `y`, `width`, and `height` are required only for `'ABSOLUTE'` positioning.
    * Plese note, for absolute positioning the pixel values for `x`, `y`, `width`, and `height` should be calculated by considering the printing paper size. The printing uses `72dpi` and for this an `A4` sized paper is `595x842` pixels.
4. `successCallback`: Called when printing is done or the print dialog is closed without printing.
5. `errorCallback`: Called with error code. The error code can be either `'INVALID_IMAGE'`, `'INVALID_FILENAME'`, or `'INVALID_POSITIONING'`.
